import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { HomeComponent } from './home/home.component';
import { AccountInfoComponent } from './account-info/account-info.component';
import { ExchangeComponent } from './exchange/exchange.component';
import { HistoricalInfoComponent } from './historical-info/historical-info.component';
import { SignInComponent } from './sign-in/sign-in.component'; 


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AccountInfoComponent,
    ExchangeComponent,
    HistoricalInfoComponent,
    SignInComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
