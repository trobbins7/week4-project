import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AccountInfoComponent } from './account-info/account-info.component';
import { ExchangeComponent } from './exchange/exchange.component';
import { HistoricalInfoComponent } from './historical-info/historical-info.component';
import { SignInComponent } from './sign-in/sign-in.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path:'AccountInfo', component: AccountInfoComponent},
  {path:'Exchange',component:ExchangeComponent},
  {path:'HistoricalInfo',component:HistoricalInfoComponent},
  {path:'SignIn',component:SignInComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
